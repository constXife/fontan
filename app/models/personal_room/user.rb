# == Schema Information
#
# Table name: personal_room.users
#
#  id                     :integer          not null, primary key
#  email                  :string
#  encrypted_password     :string           default(""), not null
#  reset_password_token   :string
#  reset_password_sent_at :datetime
#  remember_created_at    :datetime
#  sign_in_count          :integer          default(0), not null
#  current_sign_in_at     :datetime
#  last_sign_in_at        :datetime
#  current_sign_in_ip     :inet
#  last_sign_in_ip        :inet
#  provider               :string
#  uid                    :string
#  first_name             :string
#  last_name              :string
#  nickname               :string
#  avatar                 :string
#  sex                    :integer
#  avatar_uid_url         :string
#  url                    :string
#  created_at             :datetime
#  updated_at             :datetime
#

module PersonalRoom
  class User < ActiveRecord::Base
    rolify role_cname: 'PersonalRoom::Role',
           role_table_name: 'personal_room.roles',
           role_join_table_name: 'personal_room.users_roles'
    devise :omniauthable, omniauth_providers: [:vkontakte]
    mount_uploader :avatar, ::AvatarUploader

    include Concerns::Model

    has_many :cards, class_name: 'Sandglass::Card', dependent: :nullify
    has_many :card_activities, class_name: 'Sandglass::CardActivity', through: :cards

    scope :online, -> { joins(:card_activities).where(card_activities: {end_at: nil}) }

    def to_s
      full_name
    end

    def stop_activity!
      online_card.stop_activity!
    end

    def online?
      card_activities.where(card_activities: {end_at: nil}).count > 0
    end

    def online_card
      online_activities = card_activities.uncompleted
      if online_activities
        online_activities.first.card
      end
    end

    def elapsed
      cards.collect(&:elapsed).reduce(:+)
    end

    def visits
      card_activities.completed
    end

    def full_name
      [first_name, nickname, last_name].compact.reject(&:empty?).join(' ')
    end

    def update_from_omniauth(auth)
      self.first_name = auth.info.first_name
      self.last_name = auth.info.last_name
      self.nickname = auth.info.nickname
      self.sex = auth.extra.raw_info.sex
      self.url = auth.info.urls.to_hash['Vkontakte']

      if self.avatar_uid_url != auth.extra.raw_info.photo_100
        self.avatar_uid_url     = auth.extra.raw_info.photo_100.gsub('https', 'http')
        self.remote_avatar_url  = self.avatar_uid_url
      end

      save!
    end
  end
end
