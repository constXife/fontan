# == Schema Information
#
# Table name: personal_room.roles
#
#  id            :integer          not null, primary key
#  name          :string
#  resource_id   :integer
#  resource_type :string
#  created_at    :datetime
#  updated_at    :datetime
#

module PersonalRoom
  class Role < ActiveRecord::Base
    scopify

    include Concerns::Model

    has_and_belongs_to_many :users, class_name: 'PersonalRoom::User', join_table: 'personal_room.users_roles'
    belongs_to :resource, :polymorphic => true
  end
end
