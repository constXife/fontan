module PersonalRoom
  module Concerns
    module Model
      extend ActiveSupport::Concern

      included do
        self.table_name = "personal_room.#{self.table_name}"
      end
    end
  end
end
