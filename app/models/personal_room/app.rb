# == Schema Information
#
# Table name: personal_room.apps
#
#  id         :integer          not null, primary key
#  name       :string           not null
#  token      :string           not null
#  user_id    :integer
#  deleted_at :datetime
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

module PersonalRoom
  class App < ActiveRecord::Base
    rolify
    acts_as_paranoid
    has_secure_token :token

    include Concerns::Model

    belongs_to :user

    validates_presence_of :name, :user_id
  end
end
