module PersonalRoom
  class Ability
    include CanCan::Ability

    def initialize(client)
      if client.is_a?(App)
        can :manage, :all
      else
        client ||= User.new

        if client.has_role? :admin
          can :manage, :all
        else
          can :read, :all
        end
      end
    end
  end
end
