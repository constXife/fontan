module PersonalRoom
  class Version < ActiveRecord::Base
    include Concerns::Model
    include PaperTrail::VersionConcern
    self.abstract_class = true
  end
end
