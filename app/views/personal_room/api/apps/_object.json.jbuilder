json.(app, :id, :name, :token, :user_id, :created_at, :updated_at)

json.user do
  json.id app.user.id
  json.full_name app.user.full_name
end