json.(user, :id, :full_name, :avatar, :first_name, :last_name, :nickname, :sex, :elapsed)
json.roles user.roles.collect(&:name)
json.is_online user.online?

if user.online?
  json.online do
    json.card do
      json.partial! 'personal_room/api/cards/object', card: user.online_card
      json.active_since user.online_card.active_session.try(:created_at)
    end
  end
end

json.visits do
  json.times  user.visits.count
  if user.visits.count > 0
    json.last do
      json.end_at user.visits.last.end_at
      json.duration user.visits.last.duration
    end
  end
end
