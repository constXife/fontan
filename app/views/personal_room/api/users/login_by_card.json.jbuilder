json.user do
  json.partial! 'object', user: @user
end

json.card do
  json.number         @card.number
  json.active_since   @card.active_session.try(:created_at)
end