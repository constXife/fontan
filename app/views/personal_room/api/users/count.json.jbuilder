json.users do
  json.count   @users[:count]
  json.online  @users[:online]
end