json.(card, :number, :description, :weight, :user_id, :created_at, :updated_at)

json.user do
  json.id        PersonalRoom::User.find(card.user_id).id
  json.full_name PersonalRoom::User.find(card.user_id).full_name
end