"use strict";

angular.module('fontan')
  .controller('AdminUsersOnlineCtl', ['$scope', '$log', 'User',
    function($scope, $log, User) {
      $scope.isLoading = true;
      $scope.users = User.index({online: true});

      $scope.users.$promise.then(function() {
        $scope.isLoading = false;
      });
    }]);