"use strict";

angular.module('fontan')
  .controller('AdminUsersMenuCtl', ['$rootScope', 'CardTerminalEvent', '$scope', '$log', '$state', 'localStorageService', 'User', '$location',
    function($rootScope, CardTerminalEvent, $scope, $log, $state, localStorageService, User, $location) {
      $scope.isLoading = true;
      $scope.users = User.count();

      $scope.decide = function() {
        if (localStorageService.get('incomingUser')) {
          $scope.users.reception = 1;
        } else {
          $scope.users.reception = null;
        }

        if ($scope.users.reception == 1) {
          $state.go('admin.users.reception');
        } else if ($scope.users.reception == 0 && $scope.users.online > 0) {
          $state.go('admin.users.online');
        } else {
          $state.go('admin.users.index');
        }
      };

      $rootScope.$on('$stateChangeStart',
        function(event, toState, _toParams, _fromState, _fromParams){
          if (toState.name == 'admin.users.index' || toState.name == 'admin.users.online') {
            User.count().$promise.then(function(result) {
              $scope.users = result;
            });
          }
      });

      $scope.users.$promise.then(function() {
        $scope.isLoading = false;
        $scope.decide();
      });

      CardTerminalEvent.listen('incoming', function() {
        $scope.users.reception = 1;
      });

      CardTerminalEvent.listen('clear', function() {
        $scope.decide();
      });

      $scope.decide();
    }]);