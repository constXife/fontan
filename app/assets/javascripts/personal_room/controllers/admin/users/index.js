"use strict";

angular.module('fontan')
  .controller('AdminUsersIndexCtl', ['$scope', '$log', 'User',
    function($scope, $log, User) {
      $scope.isLoading = true;
      $scope.users = User.index();

      $scope.users.$promise.then(function() {
        $scope.isLoading = false;
      });

      $scope.columns = [{ field: 'full_name', name: 'Имя' }];
      $scope.gridOptions = {
        data: $scope.users,
        columnDefs: $scope.columns
      }
    }]);