"use strict";

angular.module('fontan')
  .controller('AdminUsersNewCtl', ['$scope', '$log', '$state', 'User',
    function($scope, $log, $state, User) {
      $scope.isLoading = false;
      $scope.user = new User();

      $scope.submit = function() {
        $log.debug('submit');

        var success = function() {
          $state.go('admin.users');
        };

        var failure = function(result) {
          $log.error(result);
        };

        User.create({user: $scope.user}, success, failure);
      }
    }]);