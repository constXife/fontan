"use strict";

angular.module('fontan')
  .controller('AdminUsersShowCtl', ['$scope', '$log', '$state', '$stateParams', 'User',
    function($scope, $log, $state, $stateParams, User) {
      $scope.isLoading = true;
      $scope.user = User.show({id: $stateParams.userId});

      $scope.user.$promise.then(function() {
        $scope.isLoading = false;
      });

      $scope.stop = function() {
        var success = function() {
          $state.go('admin.users.online');
        };

        var failure = function() {
          $log.error('NOT OK');
        };

        User.stop_activity({id: $stateParams.userId}, success, failure);
      };
    }]);