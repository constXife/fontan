"use strict";

angular.module('fontan')
  .controller('AdminUsersReceptionCtl', ['$scope', '$log', '$state', 'localStorageService', 'User', 'Card', 'CardTerminalEvent',
    function($scope, $log, $state, localStorageService, User, Card, CardTerminalEvent) {
      $scope.incomingUser = localStorageService.get('incomingUser');

      CardTerminalEvent.listen('incoming', function(event, user) {
        $log.debug('Reception Catch Card');

        $scope.users.push(user);
      });

      $scope.cancel = function() {
        $log.debug('Cancelling card: ' + $scope.incomingUser.card.number);
        $scope.clearIncoming();
      };

      $scope.accept = function() {
        $log.debug('incoming card: ' + $scope.incomingUser.card.number);

        var success = function() {
          $scope.clearIncoming();
          $state.go('admin.users.online');
        };

        var failure = function() {
          $log.error('NOT OK');
        };

        Card.start_activity({number: $scope.incomingUser.card.number}, success, failure);
      };

      $scope.stop = function() {
        $log.debug('incoming card: ' + $scope.incomingUser.card.number);

        var success = function() {
          $log.info('OK');
          $scope.clearIncoming();
          $state.go('admin.users');
        };

        var failure = function() {
          $log.error('NOT OK');
        };

        Card.stop_activity({number: $scope.incomingUser.card.number}, success, failure);
      };

      $scope.clearIncoming = function() {
        $scope.incomingUser = undefined;
        CardTerminalEvent.broadcast('clear');
      };
    }]);