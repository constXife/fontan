"use strict";

angular.module('fontan')
  .controller('GuestsIndexCtl', ['$scope', '$log', '$state', 'localStorageService', 'User', 'Card', 'CardTerminalEvent',
    '$timeout', '$interval',
    function($scope, $log, $state, localStorageService, User, Card, CardTerminalEvent, $timeout, $interval) {
      $scope.users = User.index({online: true});

      CardTerminalEvent.listen('incoming', function(event, user) {
        $log.debug('Reception Catch Card');

        if (user != null) {
          var nonExists = true;

          $.each($scope.users, function( index, value ) {
            if (value.id == user.id) {
              nonExists = false
            }
          });

          if (nonExists) {
            $timeout(function() {
              $scope.users.push(user);
            }, 0);
          } else {
            nonExists = true;
          }
        }
      });

      $scope.cancel = function(user) {
        $log.debug('Cancelling card:');

        $scope.removeUser(user);
        $scope.clearIncoming(user);
      };

      $scope.accept = function(user) {
        $log.debug('Incoming card:');

        var success = function() {
          $scope.clearIncoming(user);
        };

        var failure = function() {
          $log.error('NOT OK');
        };

        Card.start_activity({number: user.card.number}, success, failure);
      };

      $scope.stop = function(user) {
        $log.debug('incoming card: ' + user.card.number);

        var success = function() {
          $log.info('OK');
          $scope.clearIncoming(user);
        };

        var failure = function() {
          $log.error('NOT OK');
        };

        Card.stop_activity({number: user.card.number}, success, failure);
      };

      $scope.clearIncoming = function(user) {
        $scope.deselectUser(user);
        CardTerminalEvent.broadcast('clear', user);
      };

      $scope.selectUser = function(user) {
        $scope.selectedUser = user;
        $timeout(function() {
          $('#countdown').html(moment($scope.selectedUser.card.active_since).countdown().toString());
        }, 0);
        $interval(function() {
          $('#countdown').html(moment($scope.selectedUser.card.active_since).countdown().toString());
        }, 1000);

        $log.debug(user);
      };

      $scope.deselectUser = function() {
        $scope.selectedUser = null;
      };

      $scope.removeUser = function(user) {
        _.remove($scope.users, function(u) {
          return u.id == user.id;
        });

        if ($scope.users.length == 0) {
          $scope.users = [];
        }
      };
    }]);