"use strict";

angular.module('fontan')
  .controller('AppsIndexCtl', ['$scope', '$log', 'App',
    function($scope, $log, App) {
      $scope.isLoading = true;
      $scope.apps = App.index();

      $scope.apps.$promise.then(function() {
        $scope.isLoading = false;
      });
    }]);