"use strict";

angular.module('fontan')
  .controller('AppsNewCtl', ['$scope', '$log', 'App',
    function($scope, $log, App) {
      $scope.isLoading = false;
      $scope.app = new App();

      $scope.submit = function() {
        $log.debug('submit');

        var success = function() {
          $log.success('ok');
        };

        var failure = function(result) {
          $log.error(result);
        };

        App.create({app: $scope.app}, success, failure);
      }
    }]);