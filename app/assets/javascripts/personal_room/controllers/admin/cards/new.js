"use strict";

angular.module('fontan')
  .controller('CardsNewCtl', ['$scope', '$log', 'User', 'Card',
    function($scope, $log, User, Card) {
      $scope.isLoading = false;
      $scope.users = User.index();
      $scope.card = new Card();

      $scope.submit = function() {
        $log.debug('submit');

        var success = function() {
          $log.success('ok');
        };

        var failure = function(result) {
          $log.error(result);
        };

        Card.create({card: $scope.card}, success, failure);
      }
    }]);