"use strict";

angular.module('fontan')
  .controller('CardsIndexCtl', ['$scope', '$log', 'Card',
    function($scope, $log, Card) {
      $scope.isLoading = true;
      $scope.cards = Card.index();

      $scope.cards.$promise.then(function() {
        $scope.isLoading = false;
      });

      $scope.columns = [{ field: 'full_name', name: 'Имя' }];
      $scope.gridOptions = {
        data: $scope.cards,
        columnDefs: $scope.columns
      }
    }]);