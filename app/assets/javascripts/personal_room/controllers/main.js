"use strict";

angular.module('fontan')
  .controller('MainCtl', ['$scope', 'config', '$window', 'CardTerminalEvent', function($scope, config, $window) {
    $scope.logout = function() {
      $window.location.href = config.links.logout;
    };

    $scope.root = function() {
      $window.location.href = config.links.root;
    };

    $scope.back_to_site = function() {
      $window.location.href = config.links.mainSite;
    };
  }]);
