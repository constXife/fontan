
angular.module("fontan.services").filter('duration', function() {
  return function (input) {
    if (!input) {
      input = 0;
    }
    return moment.duration(input, "seconds").format("h [ч.]");
  };
});

angular.module("fontan.services").filter('countdown', function() {
  return function (input) {
    if (!input) {
      input = 0;
    }
    return moment(input).countdown().toString();
  };
});