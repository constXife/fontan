
angular.module("fontan.services").filter('moment', function() {
  return function (input) {
    return moment(input).format('LLL');
  };
});
