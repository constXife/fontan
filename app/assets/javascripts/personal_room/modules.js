//= require_self
//= require_tree ./services

angular.module('fontan.services', []);
angular.module('fontan.constants', []);
angular.module('fontan.namespace', ['fontan.constants', 'fontan.services']);
angular.module('fontan', [
  'fontan.namespace', 'ngAnimate', 'ngResource', 'ui.router', 'templates',
  'angularMoment', 'LocalStorageModule', 'ui.grid'
]);
