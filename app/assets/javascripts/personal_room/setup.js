"use strict";

angular.module('fontan')
  .config(
    ['$httpProvider', '$stateProvider', '$urlRouterProvider', '$logProvider', 'config', 'routes', 'localStorageServiceProvider',
    function($httpProvider, $stateProvider, $urlRouterProvider, $logProvider, config, routes, localStorageServiceProvider) {
    $httpProvider.defaults.headers.common.Accept = 'application/json';

    $urlRouterProvider.when('/', '/profile');
    $urlRouterProvider.when('', '/profile');

    $stateProvider = routes.get($stateProvider);

    localStorageServiceProvider.setPrefix('fontan');
    localStorageServiceProvider.setNotify(true, true);

    $logProvider.debugEnabled(config.is_development);
  }
]);

angular.module('fontan').run(['$rootScope', '$state', '$stateParams', '$log', 'User', function($rootScope, $state, $stateParams, $log, User) {
  $rootScope.$state = $state;
  $rootScope.$stateParams = $stateParams;
  $rootScope.current_user = User.current();

  $rootScope.$on("$stateChangeStart", function (event, toState, toParams, fromState, fromParams) {
    if ($rootScope.stateChangeBypass) {
      $rootScope.stateChangeBypass = false;
      return;
    }

    if (toState.permission != undefined) {
      event.preventDefault();

      $rootScope.current_user.$promise.then(function() {
        if ($rootScope.current_user.roles.indexOf(toState.permission) == 0) {
          $rootScope.stateChangeBypass = true;
          $state.go(toState, toParams);
        }
      });
    }

    if (toState.redirectTo) {
      event.preventDefault();
      $state.go(toState.redirectTo, toParams);
    }
  });
}]);