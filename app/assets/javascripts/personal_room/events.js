angular.module('fontan').run(['$rootScope', '$log', 'config', 'CardTerminalEvent', 'Cable', function($rootScope, $log, config, CardTerminalEvent, Cable) {
  Cable.subscriptions.create("NotificationsChannel", {
    received: function(message) {
      message = JSON.parse(message);

      if (message.event == 'card_terminal_incoming') {
        $rootScope.$apply(function() {
          CardTerminalEvent.broadcast('incoming', message.data);
        });
      }
    }
  });
}]);