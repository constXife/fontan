"use strict";

angular.module('fontan.constants')
  .constant('routes', {
    get: function($stateProvider) {
      $stateProvider
        .state('general', {
          abstract: true,
          views: {
            "": { template: '<ui-view/>' },
            "header": { templateUrl: 'shared/header.html' }
          }
        })
        .state('dashboard', {
          url: '/dashboard',
          parent: 'general',
          views: {
            "": {
              controller: 'DashboardCtl',
              templateUrl: 'index.html'
            }
          }
        })

        .state('profile', {
          url: '/profile',
          parent: 'general',
          redirectTo: 'profile.info',
          views: {
            "": { templateUrl: 'profile/layout.html' },
            "profile_menu": { templateUrl: 'profile/menu.html' }
          }
        })
        .state('profile.info', {
          url: '/info',
          views: {
            "profile_content": {
              templateUrl: 'profile/info.html'
            }
          }
        })
        .state('profile.cards', {
          url: '/cards',
          views: {
            "profile_content": {
              templateUrl: 'profile/cards.html'
            }
          }
        })

        .state('admin', {
          url: '/admin',
          parent: 'general',
          permission: 'admin',
          redirectTo: 'admin.users.index',
          views: {
            "": { templateUrl: 'admin/layout.html' },
            "admin_menu": { templateUrl: 'admin/menu.html' }
          }
        })

        .state('admin.guests', {
          url: '/guests',
          abstract: true,
          views: {
            "admin_content": {
              template: "<ui-view/>"
            }
          }
        })
        .state('admin.guests.index', {
          url: '/list',
          views: {
            "": {
              templateUrl: 'admin/guests/index.html',
              controller: 'GuestsIndexCtl'
            }
          }
        })

        .state('admin.users.reception', {
          url: '/reception',
          views: {
            "users_admin_content": {
              templateUrl: 'admin/users/reception.html',
              controller: 'AdminUsersReceptionCtl'
            }
          }
        })
        .state('admin.users', {
          url: '/users',
          views: {
            "admin_content": {
              templateUrl: "admin/users/layout.html",
              controller: 'AdminUsersMenuCtl'
            }
          }
        })
        .state('admin.users.index', {
          url: '/list',
          views: {
            "users_admin_content": {
              templateUrl: 'admin/users/index.html',
              controller: 'AdminUsersIndexCtl'
            }
          }
        })
        .state('admin.users.show', {
          url: '/:userId',
          views: {
            "users_admin_content": {
              templateUrl: 'admin/users/show.html',
              controller: 'AdminUsersShowCtl'
            }
          }
        })
        .state('admin.users.online', {
          url: '/online',
          views: {
            "users_admin_content": {
              templateUrl: 'admin/users/online.html',
              controller: 'AdminUsersOnlineCtl'
            }
          }
        })
        .state('admin.users.new', {
          url: '/new',
          views: {
            "users_admin_content": {
              templateUrl: 'admin/users/new.html',
              controller: 'AdminUsersNewCtl'
            }
          }
        })

        .state('admin.cards', {
          url: '/cards',
          abstract: true,
          views: {
            "admin_content": {
              template: "<ui-view/>"
            }
          }
        })
        .state('admin.cards.index', {
          url: '/list',
          views: {
            "": {
              templateUrl: 'admin/cards/index.html',
              controller: 'CardsIndexCtl'
            }
          }
        })
        .state('admin.cards.new', {
          url: '/new',
          views: {
            "": {
              templateUrl: 'admin/cards/new.html',
              controller: 'CardsNewCtl'
            }
          }
        })

        .state('admin.apps', {
          url: '/apps',
          abstract: true,
          views: {
            "admin_content": {
              template: "<ui-view/>"
            }
          }
        })
        .state('admin.apps.index', {
          url: '/list',
          views: {
            "": {
              templateUrl: 'admin/apps/index.html',
              controller: 'AppsIndexCtl'
            }
          }
        })
        .state('admin.apps.new', {
          url: '/new',
          views: {
            "": {
              templateUrl: 'admin/apps/new.html',
              controller: 'AppsNewCtl'
            }
          }
        })

        .state('admin.events', {
          url: '/events',
          abstract: true,
          views: {
            "admin_content": {
              template: "<ui-view/>"
            }
          }
        })
        .state('admin.events.index', {
          url: '/list',
          views: {
            "": {
              templateUrl: 'admin/events/index.html',
              controller: 'EventsIndexCtl'
            }
          }
        })
    }
  });