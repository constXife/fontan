"use strict";

angular.module("fontan.services").factory("App", ['$resource', function($resource) {
  return $resource("/api/apps/:id", {id: '@id'},
    {
      index: {
        method: 'GET',
        responseType: 'json',
        isArray: true,
        transformResponse: function (data) {
          if (typeof data == 'string')
            data = JSON.parse(data);
          return data.apps;
        }
      },
      show: {
        method: 'GET',
        responseType: 'json',
        transformResponse: function (data) {
          if (typeof data == 'string') {
            data = JSON.parse(data);
          }
          return data.apps;
        }
      },
      create: {
        method: 'POST',
        responseType: 'json',
        transformResponse: function(data) {
          if (typeof data == 'string') {
            data = JSON.parse(data);
          }
          return data.errors || data.app;
        }
      },
      update: {
        method: 'PUT',
        responseType: 'json',
        transformResponse: function(data) {
          if (typeof data == 'string') {
            data = JSON.parse(data)
          }
          return data.errors || data.app;
        }
      },
      destroy: {
        method: 'DELETE',
        responseType: 'json',
        is_array: false
      }
    })
  }]);
