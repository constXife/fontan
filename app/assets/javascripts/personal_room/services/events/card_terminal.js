"use strict";

angular.module("fontan.services").service("CardTerminalEvent", [
  '$rootScope', '$log', 'localStorageService',
  function($rootScope, $log, localStorageService) {
    this.broadcast = function(event, data) {
      if (event == 'clear') {
        this.clearIncoming(data);
      }
      if (event == 'incoming') {
        data = this.setIncoming(data);
      }

      event = 'card_terminal:' + event;
      $log.debug('CardTerminalEvent:' + event);
      $rootScope.$broadcast(event, data)
    };

    this.listen = function(event, callback) {
      event = 'card_terminal:' + event;
      $rootScope.$on(event, callback)
    };

    this.setIncoming = function(raw_message) {
      var message = JSON.parse(raw_message);
      var user = message.user;
      user.card = message.card;

      var incomingUsers = localStorageService.get('incomingUsers');

      if (incomingUsers == null) {
        incomingUsers = [];
      } else {
        var nonExists = true;

        $.each(incomingUsers, function( index, value ) {
          if (value.id == user.id) {
            nonExists = false
          }
        });

        if (nonExists) {
          incomingUsers.push(user);
        } else {
          nonExists = true;
        }
      }

      $log.debug('CardTerminalEvent:setIncomingUser');
      localStorageService.set('incomingUsers', JSON.stringify(incomingUsers));
      return user;
    };

    this.clearIncoming = function(user) {
      $log.debug('CardTerminalEvent:removeIncomingUser');

      var incomingUsers = localStorageService.get('incomingUsers');

      if (incomingUsers.length > 0) {
        _.remove(incomingUsers, function(u) {
          return u.id == user.id;
        });

        localStorageService.set('incomingUsers', JSON.stringify(incomingUsers));
      }
    };
}]);