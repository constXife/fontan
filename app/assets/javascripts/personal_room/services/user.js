"use strict";

angular
  .module("fontan.services").factory("User", ['$resource', function($resource) {
    return $resource("/api/users/:id", {id: '@id'},
      {
        index: {
          method: 'GET',
          responseType: 'json',
          isArray: true,
          transformResponse: function (data) {
            if (typeof data == 'string')
              data = JSON.parse(data);
            return data.users;
          }
        },
        show: {
          method: 'GET',
          responseType: 'json',
          transformResponse: function (data) {
            if (typeof data == 'string')
              data = JSON.parse(data);
            return data.user;
          }
        },
        create: {
          method: 'POST',
          responseType: 'json',
          transformResponse: function(data) {
            if (typeof data == 'string') {
              data = JSON.parse(data);
            }
            return data.errors || data.user;
          }
        },
        update: {
          method: 'PUT',
          responseType: 'json',
          transformResponse: function(data) {
            if (typeof data == 'string') {
              data = JSON.parse(data)
            }
            return data.errors || data.user;
          }
        },
        current: {
          url: '/api/users/current',
          method: 'GET',
          responseType: 'json',
          transformResponse: function (data) {
            if (typeof data == 'string')
              data = JSON.parse(data);
            return data.user;
          }
        },
        stop_activity: {
          url: '/api/users/:id/stop',
          method: 'PUT',
          responseType: 'json',
          transformResponse: function(data) {
            if (typeof data == 'string') {
              data = JSON.parse(data);
            }
            return data.errors || data.user;
          }
        },
        count: {
          url: '/api/users/count',
          method: 'GET',
          responseType: 'json',
          transformResponse: function (data) {
            if (typeof data == 'string')
              data = JSON.parse(data);
            return data.users;
          }
        }
      }
    )
  }]);
