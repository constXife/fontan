"use strict";

angular
  .module("fontan.services").factory("Cable", ['config', '$log', function(config, $log) {
    return Cable.createConsumer(config.websockets_host);
  }]);
