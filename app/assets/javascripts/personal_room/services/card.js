"use strict";

angular.module("fontan.services").factory("Card", ['$resource', function($resource) {
  return $resource("/api/cards/:id", {id: '@id', number: '@number'},
    {
      index: {
        method: 'GET',
        responseType: 'json',
        isArray: true,
        transformResponse: function (data) {
          if (typeof data == 'string')
            data = JSON.parse(data);
          return data.cards;
        }
      },
      show: {
        method: 'GET',
        responseType: 'json',
        transformResponse: function (data) {
          if (typeof data == 'string') {
            data = JSON.parse(data);
          }
          return data.card;
        }
      },
      create: {
        method: 'POST',
        responseType: 'json',
        transformResponse: function(data) {
          if (typeof data == 'string') {
            data = JSON.parse(data);
          }
          return data.errors || data.card;
        }
      },
      update: {
        method: 'PUT',
        responseType: 'json',
        transformResponse: function(data) {
          if (typeof data == 'string') {
            data = JSON.parse(data)
          }
          return data.errors || data.card;
        }
      },
      start_activity: {
        url: '/api/cards/:number/start',
        method: 'POST',
        responseType: 'json',
        transformResponse: function(data) {
          if (typeof data == 'string') {
            data = JSON.parse(data);
          }
          return data.errors || data.card;
        }
      },
      stop_activity: {
        url: '/api/cards/:number/stop',
        method: 'PUT',
        responseType: 'json',
        transformResponse: function(data) {
          if (typeof data == 'string') {
            data = JSON.parse(data);
          }
          return data.errors || data.card;
        }
      }
    })
  }]);
