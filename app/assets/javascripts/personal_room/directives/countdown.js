"use strict";

angular
  .module("fontan.services")
  .directive('countdown', ['$timeout', '$interval', function ($timeout, $interval) {
    return {
      restrict: 'A',
      link: function(scope, element) {
        if (scope.user) {
          $timeout(function() {
            $(element).html(moment(scope.user.online.card.active_since).countdown().toString());
          }, 0);
          $interval(function() {
            $(element).html(moment(scope.user.online.card.active_since).countdown().toString());
          }, 1000);
        }
      }
    }
  }]);