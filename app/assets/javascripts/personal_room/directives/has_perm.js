"use strict";

angular
  .module("fontan.services")
  .directive('permission', ['$rootScope', function ($rootScope) {
    return {
      restrict: 'A',
      link: function(scope, element, attrs) {
        angular.element(element).hide();

        $rootScope.$watch('current_user.roles', function(roles) {
          if (roles != undefined) {
            if (roles.indexOf(attrs.permission) == 0) {
              angular.element(element).show();
            }
          }
        })
      }
    }
  }]);