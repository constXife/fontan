module Front
  class HomeController < ApplicationController
    def index; end

    def login
      if signed_in?
        redirect_to personal_room_root_url
      end
    end
  end
end
