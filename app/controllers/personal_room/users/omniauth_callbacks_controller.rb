module PersonalRoom
  class Users::OmniauthCallbacksController < Devise::OmniauthCallbacksController
    def vkontakte
      @user = User.find_by(provider: request.env['omniauth.auth'].provider, uid: request.env['omniauth.auth'].uid)

      unless @user
        hash = {
          provider: request.env['omniauth.auth'].provider,
          url:      request.env['omniauth.auth'].info.urls.to_hash['Vkontakte']
        }

        @user = User.find_by(hash)

        if @user
          @user.uid = request.env['omniauth.auth'].uid
        else
          return redirect_to login_path, flash: { error: 'Ваш профиль не привязан к персональной карте' }
        end
      end

      @user.update_from_omniauth(request.env['omniauth.auth'])

      sign_in_and_redirect @user, :event => :authentication #this will throw if @user is not activated
      set_flash_message(:notice, :success, :kind => 'vkontakte') if is_navigational_format?
    end

    def new_session_path *args
      personal_room_root_url *args
    end
  end
end
