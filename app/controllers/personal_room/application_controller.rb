module PersonalRoom
  class ApplicationController < ::ApplicationController
    layout 'personal_room'

    def index
      render text: '', layout: true
    end
  end
end
