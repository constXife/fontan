module PersonalRoom
  module API
    class CardsController < BaseController
      load_and_authorize_resource class: 'Sandglass::Card'

      before_action :set_card, only: [:update, :destroy, :start, :stop]

      def show; end

      def index
        @cards = Sandglass::Card.all
      end

      def create
        @card = Sandglass::Card.create(card_params)
        respond_with(@card, :created)
      end

      def start
        @card.start_activity!
        respond_with(@card, :created)
      end

      def stop
        @card.stop_activity!
      end

      def update
        @card = Sandglass::Card.update(card_params)
        respond_with(@card)
      end

      def destroy
        @card.destroy

        respond_with(@card)
      end

    private

      def card_params
        params.permit(card: [:number, :description, :weight, :user_id])[:card]
      end

      def set_card
        @card = Sandglass::Card.find_by_number(params[:id] || params[:card_id])
      end
    end
  end
end
