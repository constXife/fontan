module PersonalRoom
  module API
    class UsersController < BaseController
      load_and_authorize_resource class: 'PersonalRoom::User'

      before_action :set_user, only: [:update, :destroy, :show, :stop]
      skip_before_filter :verify_authenticity_token, :only => [:login_by_card]

      has_scope :online, :type => :boolean

      def index
        @users = apply_scopes(User).all
        respond_with(@users)
      end

      def create
        @user = User.new(user_params)
        @user.provider = 'vk'
        @user.save!

        respond_with(@user, status: :created)
      end

      def show; end

      def current
        @user = current_user

        respond_with(@user)
      end

      def count
        @users = {
          count: User.count,
          online: User.online.count
        }
      end

      def update
        respond_with(@user)
      end

      def login_by_card
        @card = Sandglass::Card.find_by!(number: params[:card_number])
        @user = PersonalRoom::User.find(@card.user_id)

        result = JSON.parse(render_to_string(template: 'personal_room/api/users/login_by_card.json'))
        ActionCable.server.broadcast 'notifications', {event: 'card_terminal_incoming', data: result.to_json}.to_json

        respond_with(@user)
      end

      def stop
        @user.stop_activity!

        respond_with(@user)
      end

      def destroy
        @user.destroy

        respond_with(@user)
      end

      private

      def user_params
        params.permit(user: [:uid])[:user]
      end

      def set_user
        @user = User.find(params[:id] || params[:user_id])
      end
    end
  end
end
