module PersonalRoom
  module API
    class AppsController < BaseController
      load_and_authorize_resource class: 'PersonalRoom::App'

      before_action :set_app, only: [:update, :destroy, :show]

      def show; end

      def index
        @apps = PersonalRoom::App.all
      end

      def create
        @app = PersonalRoom::App.new(app_params)
        @app.user = current_user
        @app.save!
        respond_with(@app, :created)
      end

      def update
        @app = PersonalRoom::App.update(app_params)
        respond_with(@app)
      end

      def destroy
        @app.destroy

        respond_with(@app)
      end

    private

      def app_params
        params.permit(app: [:name])[:app]
      end

      def set_app
        @app = PersonalRoom::App.find(params[:id] || params[:app_id])
      end
    end
  end
end
