module PersonalRoom
  class API::BaseController < ApplicationController
    respond_to :json

    rescue_from CanCan::AccessDenied do
      if current_user || current_app
        render json: {error: 'Access Denied'}, status: 403
      else
        render json: {error: 'Authenticate first'}, status: 401
      end
    end
  end
end
