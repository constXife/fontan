class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception
  after_filter :set_csrf_cookie_for_ng

protected

  def verified_request?
    super || valid_authenticity_token?(session, request.headers['X-XSRF-TOKEN'])
  end

  def current_ability
    @current_ability ||= PersonalRoom::Ability.new(current_user || current_app)
  end

  def current_app
    token = request.headers['HTTP_X_TOKEN']
    @current_app = PersonalRoom::App.find_by(token: token)
  end

  def after_sign_in_path_for(resource)
    personal_room_root_url
  end

private

  def set_csrf_cookie_for_ng
    cookies['XSRF-TOKEN'] = form_authenticity_token if protect_against_forgery?
  end
end
