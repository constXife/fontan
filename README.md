## Fontan

[![Dependency Status](https://gemnasium.com/3cb471b7cd867adc023eb815b147840d.svg)](https://gemnasium.com/569fb9340349b9ca8aa77d4bf4565be4)
[![Vexor status](https://ci.vexor.io/projects/cbdd5e24-4596-4012-8e7f-70e710faacfa/status.svg)](https://ci.vexor.io/ui/projects/cbdd5e24-4596-4012-8e7f-70e710faacfa/builds)

## Описание

CMS для антикафе.

## Начало

Устанавливаем гемы

    bundle install
    gem install foreman

Запускаем сервер

    foreman start

Заходим на сайт по URL http://localhost:3000
