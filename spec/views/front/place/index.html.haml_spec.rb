require 'rails_helper'

RSpec.describe 'front/place/index', type: :view do
  it 'displays title' do
    render

    expect(rendered).to match /О месте/
  end
end
