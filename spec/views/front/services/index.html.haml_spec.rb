require 'rails_helper'

RSpec.describe 'front/services/index', type: :view do
  it 'displays title' do
    render

    expect(rendered).to match /Услуги/
  end
end
