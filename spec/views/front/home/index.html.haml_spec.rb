require 'rails_helper'

RSpec.describe 'front/home/index', type: :view do
  it 'displays title' do
    render

    expect(rendered).to match /vk_groups/
  end
end
