require 'rails_helper'

RSpec.describe 'front/contacts/index', type: :view do
  it 'displays title' do
    render

    expect(rendered).to match /Контакты/
  end
end
