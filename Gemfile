source 'https://rubygems.org'

# Bundle edge Rails instead: gem 'rails', github: 'rails/rails'
gem 'rails', '~> 4.2'
# Use postgresql as the database for Active Record
gem 'pg'
# Use SCSS for stylesheets
gem 'sass-rails', '~> 5.0'
# Use Uglifier as compressor for JavaScript assets
gem 'uglifier', '>= 1.3.0'

# Use jquery as the JavaScript library
gem 'jquery-rails'

gem 'font-awesome-rails'

gem 'jbuilder', '~> 2.1'

gem 'puma'

gem 'turbolinks'

gem 'jazz_fingers', github: 'constXife/jazz_fingers'

# Use Capistrano for deployment
# gem 'capistrano-rails', group: :development

gem 'quiet_assets', '~> 1.1'

gem 'carrierwave', '~> 0.10'
gem 'mini_magick'

gem 'redis-rails'
gem 'responders', '~> 2.0'

gem 'has_secure_token'
gem 'annotate', '~> 2.6'

gem 'actioncable', github: 'rails/actioncable'
gem 'activerecord-colored_log_subscriber'

gem 'devise', '~> 3.5'
gem 'omniauth', '~> 1.2'
gem 'omniauth-vkontakte'
gem 'cancancan', '~> 1.9'
gem 'rolify', '~> 4.0'
gem 'paranoia', '~> 2.0'
gem 'paper_trail', '~> 4.0.0'

gem 'kaminari'
gem 'has_scope'
gem 'angular-rails-templates', '~> 0.2.0'
gem 'haml-rails'
gem 'sandglass', path: 'lib/engines/sandglass'

source 'https://rails-assets.org' do
  gem 'rails-assets-angular', '~> 1.4'
  gem 'rails-assets-angular-resource', '~> 1.4'
  gem 'rails-assets-angular-animate', '~> 1.4'
  gem 'rails-assets-ui-router', '~> 0.2'
  gem 'rails-assets-semantic-ui', '~> 2.0'
  gem 'rails-assets-loaders.css', '~> 0.1'
  gem 'rails-assets-angular-file-upload', '~> 2.1'
  gem 'rails-assets-noty', '~> 2.3'
  gem 'rails-assets-animate-css', '~> 3.4'
  gem 'rails-assets-lodash', '~> 3.9'
end

group :development, :test do
  # Call 'byebug' anywhere in the code to stop execution and get a debugger console
  gem 'byebug'

  # Spring speeds up development by keeping your application running in the background. Read more: https://github.com/rails/spring
  gem 'spring'

  gem 'rspec-rails', '~> 3.0'

  gem 'rails_best_practices'
  gem 'factory_girl_rails'
  gem 'simplecov'
end

group :development do
  gem 'airbrussh',        require: false
  gem 'capistrano-rails', require: false
  gem 'capistrano3-puma', github: 'seuros/capistrano-puma'
end
