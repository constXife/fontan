$:.push File.expand_path('../lib', __FILE__)

# Maintain your gem's version:
require 'sandglass/version'

# Describe your gem and declare its dependencies:
Gem::Specification.new do |s|
  s.name        = 'sandglass'
  s.version     = Sandglass::VERSION
  s.authors     = ['Karim Topoev']
  s.email       = ['constxife@yandex.ru']
  s.homepage    = 'http://constxife.ru'
  s.summary     = 'Карточная система'
  s.description = 'Карточная система'
  s.license     = 'commercial'

  s.files = Dir['{app,config,db,lib}/**/*', 'Rakefile', 'README.md']

  s.add_dependency 'rails', '~> 4.2'
  s.add_dependency 'paranoia'
  s.add_dependency 'paper_trail'

  s.add_development_dependency 'pg'
  s.add_development_dependency 'annotate'
  s.add_development_dependency 'migration_comments'
  s.add_development_dependency 'rails_best_practices'
  s.add_development_dependency 'factory_girl_rails'
  s.add_development_dependency 'rspec-rails'
end
