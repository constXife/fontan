module Sandglass
  class Engine < ::Rails::Engine
    isolate_namespace Sandglass

    initializer :append_migration_paths do |app|
      Sandglass::Engine.config.paths['db/migrate'].expanded.each do |expanded_path|
        app.config.paths['db/migrate'] << expanded_path
      end
    end
  end
end
