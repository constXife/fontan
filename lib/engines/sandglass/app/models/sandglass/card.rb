# == Schema Information
#
# Table name: cards
#
#  id          :integer          not null, primary key
#  number      :string           not null
#  description :text
#  weight      :integer
#  user_id     :integer
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#
# Indexes
#
#  index_cards_on_user_id  (user_id)
#

module Sandglass
  class Card < ActiveRecord::Base
    self.table_name = 'sandglass.cards'

    has_many :card_activities, dependent: :destroy

    def to_param
      number
    end

    def active?
      !!active_session
    end

    def start_activity!
      card_activities.build.start!
    end

    def stop_activity!
      unless active_session
        fail "not found started card activity: #{self.number}"
      end

      active_session.stop!
    end

    def elapsed
      sessions.collect(&:duration).reduce(:+)
    end

    def active_session
      card_activities.find_by(end_at: nil)
    end

  private

    def sessions
      card_activities.where.not(end_at: nil)
    end
  end
end
