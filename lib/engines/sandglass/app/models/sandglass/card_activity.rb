# == Schema Information
#
# Table name: card_activities
#
#  id         :integer          not null, primary key
#  end_at     :datetime
#  card_id    :integer
#  created_at :datetime         not null
#  updated_at :datetime         not null
#
# Indexes
#
#  index_card_activities_on_card_id  (card_id)
#

module Sandglass
  class CardActivity < ActiveRecord::Base
    self.table_name = 'sandglass.card_activities'

    belongs_to :card

    scope :completed,     -> { where.not(end_at: nil) }
    scope :uncompleted,   -> { where(end_at: nil) }

    def start!
      save! unless persisted?
    end

    def stop!
      self.end_at = Time.now
      save!
    end

    def duration
      ((self.end_at - self.created_at) / 1.seconds).round
    end
  end
end
