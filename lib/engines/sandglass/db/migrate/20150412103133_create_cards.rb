class CreateCards < ActiveRecord::Migration
  def change
    create_table :'sandglass.cards' do |t|
      t.string      :number,      null: false,  comment: 'Number of the card'
      t.text        :description, null: true,   comment: 'Some description'
      t.integer     :weight,      null: true,   comment: 'Weight for importance'
      t.belongs_to  :user,        null: true,   comment: 'Owner', index: true

      t.timestamps null: false
    end
  end
end
