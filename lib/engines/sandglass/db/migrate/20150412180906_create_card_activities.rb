class CreateCardActivities < ActiveRecord::Migration
  def change
    create_table :'sandglass.card_activities' do |t|
      t.datetime :end_at,   null: true
      t.belongs_to :card,   index: true

      t.timestamps null: false
    end

    add_foreign_key :'sandglass.card_activities', :'sandglass.cards', column: 'card_id', on_delete: :cascade
  end
end
