Rails.application.routes.draw do
  scope module: :personal_room do
    scope '/api', module: :api, defaults: { format: 'json' } do
      resources :users do
        get 'current' => 'users#current', on: :collection
        get 'count'   => 'users#count',   on: :collection
        post 'login_by_card/:card_number' => 'users#login_by_card', on: :collection

        put 'stop' => 'users#stop'
      end

      resources :cards do
        post  '/start'  => 'cards#start'
        put   '/stop'   => 'cards#stop'
      end

      resources :apps
    end
  end

  scope :personal_room do
    devise_for :users,
               controllers: {
                 omniauth_callbacks: 'personal_room/users/omniauth_callbacks'
               },
               class_name: 'PersonalRoom::User',
               skip: [:sessions, :registrations, :passwords]

    devise_scope :user do
      get 'logout' => 'devise/sessions#destroy', :as => :destroy_user_session
    end

    get '/' => 'personal_room/application#index', as: :personal_room_root
  end

  scope module: :front do
    get '/'         => 'home#index',      as: :homepage
    get 'login'     => 'home#login',      as: :login
    get 'place'     => 'place#index',     as: :place
    get 'services'  => 'services#index',  as: :services
    get 'contacts'  => 'contacts#index',  as: :contacts
  end

  root 'home_controller#index'
end
