# config valid only for current version of Capistrano
lock '3.4.0'

set :application, 'fontan'
set :repo_url, 'git@gitlab.com:constXife/fontan.git'

set :deploy_to, '/home/fontan/fontan'

set :linked_files, fetch(:linked_files, []).push(
                   'config/database.yml', 'config/secrets.yml', 'config/application.yml', 'config/redis.yml',
                   'config/newrelic.yml', 'config/redis/cable.yml'
                 )
set :linked_dirs, fetch(:linked_dirs, []).push(
                  'bin', 'log', 'tmp/pids', 'tmp/cache', 'tmp/sockets', 'vendor/bundle', 'public/system',
                  'public/uploads'
                )

set :keep_releases, 1
set :rails_env, 'production'

set :puma_conf,   -> { File.join(shared_path, 'config/deploy/puma.rb') }
set :puma_pid,    -> { File.join(shared_path, 'tmp/pids/fontan.pid') }
set :puma_state,  -> { File.join(shared_path, 'tmp/pids/fontan.state') }

namespace :deploy do
  desc 'Copy assets'
  after 'deploy:updating', :copy_assets do
    run_locally do
      with rails_env: :production do
        execute :bundle, 'exec rake assets:precompile assets:clean'
      end
    end

    local_manifest_path = %x{ls public/assets/manifest*}.strip

    on roles(:app) do |server|
      warn "#{server}"
      %x{rsync -av ./public/assets/ #{server.user}@#{server.hostname}:#{release_path}/public/assets/}
      %x{rsync -av ./#{local_manifest_path} #{server.user}@#{server.hostname}:#{release_path}/assets_manifest#{File.extname(local_manifest_path)}}
      execute :chmod, "-R +r #{release_path}/public/assets"
    end

    run_locally do
      execute :bundle, 'exec rake assets:clobber'
    end
  end

  desc 'Start application'
  task :start do
    on roles(:app), in: :sequence, wait: 5 do
    end
  end

  desc 'Stop application'
  task :stop do
    on roles(:app), in: :sequence, wait: 5 do
    end
  end

  desc 'Restart application'
  task :restart do
    on roles(:app), in: :sequence, wait: 5 do
    end
  end

  after :publishing, :restart

  after :restart, :clear_cache do
    on roles(:web), in: :groups, limit: 3, wait: 10 do
      # Here we can do anything such as:
      # within release_path do
      #   execute :rake, 'cache:clear'
      # end
    end
  end

end

namespace :rails do
  desc 'Remote console'
  task :console do
    on roles(:app) do |h|
      run_interactively "bundle exec rails console -e #{fetch(:rails_env)}", h.user
    end
  end

  desc 'Remote dbconsole'
  task :dbconsole do
    on roles(:app) do |h|
      run_interactively "bundle exec rails dbconsole -e #{fetch(:rails_env)}", h.user
    end
  end

  def run_interactively(command, user)
    info "Running `#{command}` as #{user}@#{host}"
    exec %Q(ssh #{user}@#{host} -t "bash --login -c 'cd #{fetch(:deploy_to)}/current && #{command}'")
  end
end
