class RolifyCreateRoles < ActiveRecord::Migration
  def change
    create_table(:'personal_room.roles') do |t|
      t.string :name
      t.references :resource, :polymorphic => true

      t.timestamps
    end

    create_table(:'personal_room.users_roles', :id => false) do |t|
      t.references :user
      t.references :role
    end

    add_index(:'personal_room.roles', :name)
    add_index(:'personal_room.roles', [ :name, :resource_type, :resource_id ], name: 'roles_nrr_idx')
    add_index(:'personal_room.users_roles', [ :user_id, :role_id ])
  end
end
