class CreateApps < ActiveRecord::Migration
  def change
    create_table :'personal_room.apps' do |t|
      t.string     :name,   null: false, comment: 'App name'
      t.string     :token,  null: false, comment: 'Security token'
      t.belongs_to :user,   index: true, comment: 'Owner'

      t.datetime   :deleted_at, index: true

      t.timestamps null: false
    end

    add_foreign_key :'personal_room.apps', :'personal_room.users', column: 'user_id', on_delete: :cascade
  end
end
